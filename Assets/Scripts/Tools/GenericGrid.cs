using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct GenericGrid<T>
{
    public T[,] Grid;

    public void Initialize(IntVector2 size, T item)
    {
        Grid = new T[size.x, size.y];
        for (int i = 0; i < Grid.GetLength(0); i++)
        {
            for (int j = 0; j < Grid.GetLength(1); j++)
            {
                Grid[i, j] = item;
            }
        }
    }

    public T GetItem(IntVector2 pos) => Grid[pos.x, pos.y];

    public void SetItem(IntVector2 pos, T item) => Grid[pos.x, pos.y] = item;

    public bool InBounds(IntVector2 id)
    {
        return 0 <= id.x && id.x < Grid.GetLength(0)
            && 0 <= id.y && id.y < Grid.GetLength(1);
    }

    public IEnumerable<T> Neighbors(IntVector2 targetNode, int distance = 1)
    {
        foreach (var dir in IntVector2.directions)
        {
            var neighborNode = targetNode + dir * distance;
            if (!InBounds(neighborNode)) continue;
            var neighbor = Grid[neighborNode.x, neighborNode.y];
            {
                yield return neighbor;
            }
        }
    }

    public IEnumerable<IntVector2> NeighborsPositions(IntVector2 targetNode, int distance = 1)
    {
        foreach (var dir in IntVector2.directions)
        {
            var neighborNode = targetNode + dir * distance;
            if (!InBounds(neighborNode)) continue;
            {
                //Debug.Log(neighborNode);
                yield return neighborNode;
            }
        }
    }
    public IEnumerable<IntVector2> NeighborsPositions(IntVector2 targetNode, HashSet<IntVector2> closed, int distance = 1)
    {
        foreach (var dir in IntVector2.directions)
        {
            var neighborNode = targetNode + dir * distance;
            if (!InBounds(neighborNode) || closed.Contains(neighborNode)) continue;
            {
                //Debug.Log(neighborNode);
                yield return neighborNode;
            }
        }
    }
    public IEnumerable<IntVector2> NeighborsPositions(IntVector2 targetNode, T ignore, int distance = 1)
    {
        foreach (var dir in IntVector2.directions)
        {
            var neighborNode = targetNode + dir * distance;
            if (!InBounds(neighborNode) || Grid[neighborNode.x, neighborNode.y].Equals(ignore)) continue;
            {
                //Debug.Log(neighborNode);
                yield return neighborNode;
            }
        }
    }
    public IEnumerable<IntVector2> NeighborsPositions(IntVector2 targetNode, T ignore, T ofType, int distance = 1)
    {
        if (!Grid[targetNode.x, targetNode.y].Equals(ofType))
        {
            yield break;
        }
        foreach (var dir in IntVector2.directions)
        {
            var neighborNode = targetNode + dir * distance; ;
            if (!InBounds(neighborNode) || Grid[neighborNode.x, neighborNode.y].Equals(ignore)) continue;
            {
                //Debug.Log(neighborNode);
                yield return neighborNode;
            }
        }
    }
}
