using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class AStar
{
    public static Stack<Tile> AStarSearch(TileGrid grid, IntVector2 start, IntVector2 goal)
    {
        Stack<Tile> path = null;

        var startTile = grid.GetTile(start);
        var goalTile = grid.GetTile(goal);

        startTile.value = 0;

        var frontier = new List<Tile>();
        var closed = new HashSet<Tile>();

        frontier.Add(grid.GetTile(start));

        HashSet<Tile> closedT = new HashSet<Tile>();

        while (frontier.Count > 0)
        {
            frontier.Sort();
            var currentPos = frontier[0];
            frontier.Remove(currentPos);
            closedT.Add(currentPos);
            if (currentPos.pos == goal)
            {
                Debug.DebugBreak();
                path = new Stack<Tile>();
                while (currentPos.pos != start)
                {
                    path.Push(currentPos);
                    Debug.DrawLine(currentPos.pos, currentPos.previousTile.pos, Color.green, 0.4f);
                    currentPos = currentPos.previousTile;
                }
                return path;
            }

            foreach (var next in grid.Neighbors(currentPos))
            {
                if (next.isWalkable)
                {
                    var nextValue = currentPos.value + ManhattanDistance(next, goalTile);

                    if (!closedT.Contains(next) || nextValue < next.value)
                    {
                        Debug.DrawLine(currentPos.pos, next.pos, Color.red, 0.4f);
                        next.previousTile = currentPos;
                        TieBreakerDistance(ref nextValue, startTile, next, goalTile);
                        next.value = nextValue;
                        frontier.Add(next);
                    }
                }
            }
        }

        Debug.LogError("no path found");
        return path;
    }



    public static float ManhattanDistance(in Tile from, in Tile to)
    {
        return Mathf.Abs(to.pos.x - from.pos.x) + Mathf.Abs(to.pos.y - from.pos.y);
    }
    public static float ManhattanDistance(IntVector2 a, IntVector2 b)
    {
        return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
    }

    public static void TieBreakerDistance(ref float distance, in Tile from, in Tile current, in Tile to)
    {
        Vector2 fromTo = DistanceDiff(from, to);

        Vector2 currentTo = DistanceDiff(current, to);

        distance += Mathf.Abs(currentTo.x * fromTo.y - fromTo.x * currentTo.y) * 0.001f;
    }

    private static Vector2 DistanceDiff(in Tile from, in Tile to)
    {
        return new Vector2(from.pos.x - to.pos.x, from.pos.y - to.pos.y);
    }
}

public struct TileGrid
{
    public readonly Tile[,] Grid;

    public TileGrid(int[,] world, int nonWalkableIndex)
    {
        var graph = new Tile[world.GetLength(0), world.GetLength(1)];
        for (int i = 0; i < graph.GetLength(0); i++)
        {
            for (int j = 0; j < graph.GetLength(1); j++)
            {
                bool walkable = world[i, j] != nonWalkableIndex;
                graph[i, j] = new Tile(i, j, walkable);
            }
        }
        //Debug.Log(graph.Length);
        Grid = graph;
    }

    public Tile GetTile(IntVector2 pos) => Grid[pos.x, pos.y];


    public IEnumerable<Tile> Neighbors(IntVector2 pos)
    {
        return Neighbors(Grid[pos.x, pos.y]);
    }

    public IEnumerable<Tile> Neighbors(Tile node)
    {
        foreach (var dir in IntVector2.directions)
        {
            var nextDir = node.pos + dir;
            var neighbor = Grid[nextDir.x, nextDir.y];
            if (neighbor.isWalkable)
            {
                yield return neighbor;
            }
        }
    }
}
