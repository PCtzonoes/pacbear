using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstarNode
{
    public readonly IntVector2 pos;
    public float value = 0f;

    public AstarNode(int i, int j, bool wall)
    {
        pos.x = i;
        pos.y = j;
        if (wall) value = float.MaxValue;
        else value = 0f;
    }

    public bool isWalkable => value < float.MaxValue;
}
