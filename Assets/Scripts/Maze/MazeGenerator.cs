using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public partial class MazeGenerator : MonoBehaviour
{
    //DEPRECATED: Lab Challange
    public static int[,] Generate(IntVector2 size)
    {
        // if (goArray == null || !goArray.Contains(typeof(Wall)))
        size = (size * 2) + 1;
        Debug.Log(size);
        GenericGrid<int> gGrid = new GenericGrid<int>();
        gGrid.Initialize(size, 1);

        Queue<IntVector2> open = new Queue<IntVector2>();
        Queue<IntVector2> possible = new Queue<IntVector2>();
        HashSet<IntVector2> closed = new HashSet<IntVector2>();

        var startPos = new IntVector2(1, 1);

        open.Enqueue(startPos);
        gGrid.SetItem(startPos, 0);

        int x = 0;

        while ((open.Count > 0 || possible.Count > 0) && x < 5000)
        {
            x++;
            if (open.Count == 0) open.Enqueue(possible.Dequeue());

            IntVector2 current = open.Dequeue();
            closed.Add(current);

            var farNeighbors = gGrid.NeighborsPositions(current, closed, 2);
            int neighborsCount = farNeighbors.Count();
            Debug.Log(neighborsCount);

            if (neighborsCount > 0)
            {
                int randomized = Random.Range(0, int.MaxValue) % neighborsCount;
                IntVector2 selectedPos = farNeighbors.ElementAt(randomized);
                open.Enqueue(selectedPos);
                IntVector2 midlePos = (current + selectedPos) * 0.5f;
                gGrid.SetItem(midlePos, 0);
                gGrid.SetItem(selectedPos, 0);
                foreach (var item in farNeighbors)
                {
                    if (closed.Contains(item) || open.Contains(item) || possible.Contains(item)) continue;
                    possible.Enqueue(item);
                }
            }
        }
        Debug.Log(x);

        return gGrid.Grid;
    }

    //second version following https://www.contralogic.com/2d-pac-man-style-maze-generation/
    //idea, Need improviment
    public static int[,] GenerateV2(IntVector2 size, int honeyAmmount, int ghostAmmount)
    {
        var newSize = (size * 2) + 1;
        Debug.Log(size.x);
        GenericGrid<int> gGrid = new GenericGrid<int>();
        gGrid.Initialize(newSize, 1);

        //TODO: remove magic numbers
        BuildPoint(ref gGrid, new IntVector2(5, 5));
        BuildPoint(ref gGrid, new IntVector2(5, newSize.y - 6));
        BuildPoint(ref gGrid, new IntVector2(newSize.x - 6, 5));
        BuildPoint(ref gGrid, new IntVector2(newSize.x - 6, newSize.y - 6));

        List<IntVector2> posList = new List<IntVector2>();

        for (int i = 0; i < newSize.x; i++)
        {
            for (int j = 0; j < newSize.y; j++)
            {
                var pos = new IntVector2(i, j);
                if (gGrid.NeighborsPositions(pos, 1, 0, 1).Count() == 4)
                    posList.Add(pos);
            }
        }
        IntVector2 pacPoss = posList[0];

        foreach (var item in posList)//find pacbear avaiable positions
        {
            float elementDistance = AStar.ManhattanDistance(item, size);
            float bestDistance = AStar.ManhattanDistance(pacPoss, size);
            if (bestDistance > elementDistance) pacPoss = item;
        }

        gGrid.SetItem(pacPoss, 2);//pacbear set
        posList.Clear();//Clearpacbear avaiable positions
        for (int i = 0; i < newSize.x; i++)
        {
            for (int j = 0; j < newSize.y; j++)
            {
                var pos = new IntVector2(i, j);
                if (gGrid.Grid[i, j] == 0 && AStar.ManhattanDistance(pacPoss, pos) > 5)
                    posList.Add(pos);
            }
        }
        posList.Shuffle();

        for (int i = 0; i < posList.Count; i++)
        {
            if (i >= honeyAmmount + ghostAmmount) break;//stop on max ammount

            if (i < honeyAmmount) gGrid.SetItem(posList[i], 3);
            else gGrid.SetItem(posList[i], 4);
        }

        return gGrid.Grid;
    }
    private static void BuildPoint(ref GenericGrid<int> grid, IntVector2 start)
    {

        int builderAmmount = Random.Range(2, 5);//directions range cap
        IntVector2[] dirs = IntVector2.directions;
        dirs.Shuffle();
        grid.SetItem(start, 0);

        //HACK: temporary all directions
        for (int i = 0; i < 4/*builderAmmount*/; i++)
        {
            int count = Random.Range(1, grid.Grid.GetLength(0) >> 1);
            BuilderPath(ref grid, start, dirs[i], count);
        }
    }
    /// <summary>
    /// Recursive Builder
    /// </summary>
    /// <param name="poss">Current builder position</param>
    /// <param name="dir">Direction that the builder should go</param>
    /// <param name="count">Amount of times he should open a path</param>
    /// <returns></returns>
    private static bool BuilderPath(ref GenericGrid<int> grid, IntVector2 poss, IntVector2 dir, int count)
    {
        if (count < 0) return false;//stop
        IntVector2 currentPos = poss + dir * 2;//basic movement
        if (!grid.InBounds(currentPos)) return false;//out grid

        IntVector2 midlePos = poss + dir;
        grid.SetItem(midlePos, 0);
        if (grid.GetItem(currentPos) == 0) return false;//
        grid.SetItem(currentPos, 0);
        --count;
        //FIXME: remake this part later
        if (!BuilderPath(ref grid, currentPos, dir, count))
        {
            IntVector2[] perDir = IntVector2.PerpendicularDirection(dir);
            perDir.Shuffle();
            int randCount = Random.Range(1, grid.Grid.GetLength(0) >> 1);
            IntVector2 first = perDir[0] + currentPos;
            IntVector2 second = perDir[1] + currentPos;
            IntVector2 nextposs = currentPos + dir * 2;
            if (grid.InBounds(first))//if fist side is viable
            {
                BuilderPath(ref grid, currentPos, perDir[0], randCount);
            }
            else if (grid.InBounds(second))//secoond side viable
            {
                BuilderPath(ref grid, currentPos, perDir[1], randCount);
            }
            if (grid.InBounds(nextposs) && grid.GetItem(nextposs) != 0)//case he could walk more
            {
                BuildPoint(ref grid, nextposs);
            }
        }
        return true;
    }
}
