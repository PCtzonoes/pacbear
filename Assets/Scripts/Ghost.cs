using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : BaseUnit
{

    public PathFinder.PathType pType;
    public TileGrid grid;

    protected override void Start()
    {
        base.Start();
    }

    private void Update()
    {
        if (moveTimer == 0)
        {
            ChasePlayer();
        }
        Move();
    }

    private void ChasePlayer()
    {
        Stack<Tile> path = PathFinder.GetPath(NextPosInGrid, PacBear.Singleton.NextPosInGrid, pType, grid);

        if (path == null || path.Count == 0)
        {
            direction = IntVector2.zero;
            return;
        }

        IntVector2 destinationPos = path.Pop().pos;

        direction = destinationPos - nextPosInGrid;
    }
}
